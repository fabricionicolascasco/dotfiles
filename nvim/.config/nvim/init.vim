call plug#begin('~/.vim/plugged')
    "lsp
    Plug 'neovim/nvim-lspconfig'
    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'hrsh7th/cmp-buffer'
    Plug 'hrsh7th/nvim-cmp'
    Plug 'rust-lang/rust-analyzer'

    " Snippets
    Plug 'L3MON4D3/LuaSnip'
    Plug 'rafamadriz/friendly-snippets'

    "Neovim Telescope
    Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'nvim-telescope/telescope-fzy-native.nvim'

    " Neovim Treesitter
    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
    Plug 'nvim-treesitter/playground'

    "Harpoon
    Plug 'ThePrimeagen/harpoon'

    "colorscheme
    Plug 'gruvbox-community/gruvbox'
    Plug 'mhartington/oceanic-next'

    "Undotree
    Plug 'mbbill/undotree'

    "Autopairs
    Plug 'jiangmiao/auto-pairs'

    "Statusline
    Plug 'nvim-lualine/lualine.nvim'
    Plug 'kyazdani42/nvim-web-devicons'

    "Msc
    Plug 'ThePrimeagen/vim-be-good'

    "WakaTime
    Plug 'wakatime/vim-wakatime'

    "Nvim tree
    Plug 'kyazdani42/nvim-tree.lua'
call plug#end()

syntax enable
syntax on

let g:oceanic_next_terminal_bold = 1
let g:oceanic_next_terminal_italic = 1
colorscheme OceanicNext

let mapleader=" "

fun! TrimWhiteSpace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

augroup ncasco
    autocmd!
    autocmd BufWritePre * :call TrimWhiteSpace()
augroup END

lua require("ncasco")
