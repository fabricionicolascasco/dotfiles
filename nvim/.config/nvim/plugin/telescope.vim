"Moove between files
nnoremap <C-p> :lua require('telescope.builtin').git_files()<CR>
nnoremap <C-f> :lua require('telescope.builtin').find_files()<CR>
nnoremap <leader>fh :lua require('telescope.builtin').oldfiles()<CR>

"Search
nnoremap <leader>ps :lua require('telescope.builtin').grep_string({ search = vim.fn.input("Grep for > ")})<CR>
nnoremap <leader>pw :lua require('telescope.builtin').grep_string { search = vim.fn.expand("<cword>")} <CR>

"LSP
nnoremap <leader>gd :lua require('telescope.builtin').lsp_definitions()<CR>
nnoremap <leader>gi :lua require('telescope.builtin').lsp_implementations()<CR>
nnoremap <leader>gr :lua require('telescope.builtin').lsp_references()<CR>
nnoremap <leader>gca :lua require('telescope.builtin').lsp_code_actions()<CR>

"GIT
nnoremap <leader>gc :lua require('telescope.builtin').git_commits()<CR>
nnoremap <leader>gb :lua require('telescope.builtin').git_branches()<CR>
nnoremap <leader>gs :lua require('telescope.builtin').git_status()<CR>

"Helpers
nnoremap <leader>km :lua require('telescope.builtin').keymaps()<CR>
nnoremap <leader>vb :lua require('telescope.builtin').buffers()<CR>
nnoremap <leader>vh :lua require('telescope.builtin').help_tags()<CR>
