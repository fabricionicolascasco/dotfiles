let g:nvim_tree_indent_markers = 1
let g:nvim_tree_group_empty = 0

nnoremap <C-n> :NvimTreeToggle<CR>
nnoremap <leader>n :NvimTreeFindFile<CR>
