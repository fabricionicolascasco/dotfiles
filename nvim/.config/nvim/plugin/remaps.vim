"Scroll
nnoremap <Up> <C-y>
nnoremap <Down> <C-e>

"Switch between tabs
nnoremap <Right> gt
nnoremap <Left>  gT

"Moove between split windows
nnoremap <leader>h <C-W><C-H>
nnoremap <leader>j <C-W><C-J>
nnoremap <leader>k <C-W><C-K>
nnoremap <leader>l <C-W><C-L>
"Make splits the same size
nnoremap <leader>= <C-w>=
"Sizing windows
nnoremap <leader>- <C-W>5<
nnoremap <leader>+ <C-W>5>

"Use null register for the next delete or copy
nmap <leader>d "_d
nmap <leader>c "_c
nmap <leader>p "_p
vmap <leader>d "_d
vmap <leader>c "_c
vmap <leader>p "_p

"Moove lines
inoremap <M-j> <Esc>:m .+1<CR>==gi
inoremap <M-k> <Esc>:m .-2<CR>==gi
vnoremap <M-j> :m '>+1<CR>gv=gv
vnoremap <M-k> :m '<-2<CR>gv=gv

"Make Y behave like C and D
"nnoremap Y y$

"Keep cursor centered when jumping
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

"Special characters
inoremap <M-n> ñ
inoremap <M-N> Ñ
inoremap <M-a> á
inoremap <A-e> é
inoremap <M-i> í
inoremap <M-o> ó
inoremap <M-u> ú
inoremap <M-A> Á
inoremap <M-E> É
inoremap <M-I> Í
inoremap <M-O> Ó
inoremap <M-U> Ú
inoremap ,shrug ¯\_(ツ)_/¯

"Undo break points
inoremap , ,<C-g>u
inoremap . .<C-g>u
inoremap ! !<C-g>u
inoremap ? ?<C-g>u

"Jumplist mutations
nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'
