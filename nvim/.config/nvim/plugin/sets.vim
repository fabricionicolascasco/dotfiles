set exrc
"set guicursor=
set relativenumber nu
set nohlsearch
set hidden
set noerrorbells
"set shiftwidth=4 esto deberia depender del proyecto vscode=> 2 vstudio => 4
set shiftwidth=2
set expandtab
set smartindent
set nowrap
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set ignorecase
set smartcase
set incsearch
set termguicolors
set scrolloff=8
set noshowmode
set completeopt=menuone,noinsert,noselect
set colorcolumn=80
set signcolumn=yes
set cmdheight=2
set updatetime=50
set shortmess+=c
